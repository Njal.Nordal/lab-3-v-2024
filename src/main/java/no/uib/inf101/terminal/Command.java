package no.uib.inf101.terminal;

public interface Command {

    public String run(String[] args);

    public String getName();
    
    default void setContext(Context context) { /* do nothing */ };

    
}
